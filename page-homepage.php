<?php
get_header(); ?>

    <!-- Sessão 1 - Edições -->

    <div id="edicoes">
        <?php get_template_part('template/sessao', 'edicoes') ?>
    </div>

    <!-- Sessão 2 - Noticias + Publicidade -->

    <div id="noticias" class="sessao blog">
        <?php get_template_part('template/sessao', 'noticias') ?>
    </div>

    <!-- Sessão 4 - Edição atual -->  

    <div id="edicao-atual" class="sessao">
        <?php get_template_part('template/sessao', 'edicao-atual') ?>
    </div>

    <!-- Sessão 5 - Edição atual -->  

    <div id="social" class="sessao blog">
        <div class="conteudo">

            <?php get_template_part('template/sessao', 'youtube') ?>

            <?php get_template_part('template/sessao', 'ad-blog') ?>

            <?php get_template_part('template/sessao', 'edicao-anterior') ?>

            <?php get_template_part('template/sessao', 'blog') ?>

        </div>
        <div class="sidebar">
            <?php get_template_part('template/sessao', 'sidebar') ?>
        </div>
        
    </div>

<?php
get_footer();