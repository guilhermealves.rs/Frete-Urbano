<?php

get_header(); ?>

<div class="cat-<?php global $post; echo $post->post_name;?> interna categoria">
	
	<div class="conteudo-interno-blog">
 
	<?php
		while ( have_posts() ) :
			the_post();
?>
        <article class="<?php post_class() ?>">
            <h1><?php the_title() ?></h1>
            <div class="conteudo">
                <div class="img">
                    <?php echo the_post_thumbnail( 'medium' ) ?>
                </div>
                <div class="resumo">
                    <?php the_excerpt() ?>
                    <a href="<?php the_permalink() ?>">Leia mais</a>
                </div>
            </div>
        </article>
    <?php
                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>


    </div>
    <?php get_template_part('template/sidebar', 'interna') ?>
</div>
<?php
get_footer();