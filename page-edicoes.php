<?php

get_header(); ?>

<div class="<?php global $post; echo $post->post_name;?> interna">
	</header>
	
	<div class="conteudo-interno-blog">
    <div class="<?php post_class() ?>">

<h1><?php the_title() ?></h1>

	<?php
		while ( have_posts() ) :
			the_post();
?>

<?php the_content() ?>
<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
</div>
<?php
    $taxonomyName = "region";
    //Could use ACF or basic custom field to get the "parent tax ID" dynamically from a page. At least that's what I would do.
    $parent_tax_ID = '3';
    $parent_tax = get_term($parent_tax_ID);
    echo '<h3>' . $parent_tax->name . '</h3>';
    echo '<ul>';
    $terms = get_terms( $taxonomyName, array( 'parent' => $parent_tax_ID, 'orderby' => 'slug', 'hide_empty' => false ) );
    foreach ( $terms as $term ) {
        echo '<li><a href="' . get_term_link( $term ) . '">' . $term->name . '</a></li>';
    }
    echo '</ul>';
?>
</div>
<div class="sidebar-interna">
   <div class="noticias">
   <div class="box">
			<div class="heading">
				<h2><a href="#">Publicidade</a></h2>
			</div>
		</div>
     <div class="box">
            <div class="heading">
                <h2><a href="#">Noticias</a></h2>
            </div>
			<div class="conteudo">
			<?php $loops = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 1 ) ); ?>
            <?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;
            ?>
                <div class="item destaque">
					<a href="<?php the_permalink()?>">	
                    <div class="img">
                        <img src="<?php the_post_thumbnail_url('medium-large')?>" alt="<?php the_title() ?>">
                    </div>
                    <div class="informacoes">
                        <div class="titulo">
                            <h2><?php the_title() ?></h2>
                        </div>
                        <p class="resumo">
                           <?php echo excerpt(25) ?>
                        </p>
					</div>
				</a>
                </div>
				<?php endwhile; wp_reset_query(); ?>
				<?php $loops = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 3, 'offset' => 1) ); ?>
				<?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;
				?>
                <div class="item">
					<a href="<?php the_permalink()?>">
                    <div class="img">
                        <img src="<?php the_post_thumbnail_url('medium-large')?>" alt="<?php the_title() ?>">
                    </div>
                    <div class="informacoes">
                        <div class="titulo">
                            <h2><?php the_title() ?></h2>
                        </div>
					</div>
					</a>
                </div>
				<?php endwhile;?>
            </div>
        </div>

   </div>
</div>
</div>
<?php
get_footer();