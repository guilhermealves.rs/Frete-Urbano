<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<script src="https://use.fontawesome.com/51e62760a1.js"></script> -->
<?php wp_head(); ?>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Play" rel="stylesheet">
<link rel="icon" href="<?php echo get_template_directory_uri();?>/img/favicon.png" type="image/x-icon" />
<?php 
    $excerpt = get_excerpt(); 
    $info = get_bloginfo('description');
    $image = get_the_post_thumbnail_url();
    if(!empty($excerpt)): 
    $resumo = $excerpt;
    else: 
    $resumo = $info;
    endif;

    $mtitle_default = get_bloginfo('name');
    $title_default  = get_bloginfo('name');
    $home_default   = get_home_url();
   
    $text = get_option('general_opts');
    $text = $text['tags'];

    $keys_default   = $text;
    $link_default   = get_home_url();
    $desc_default   = get_bloginfo('description');
    $image_default  = get_template_directory_uri() . '/assets/img/logo.png';
    if (is_single() || is_page()) {
        $title_default = get_the_title($post->ID);
        $link_default  = get_permalink();
        if(has_post_thumbnail()){
            $image_ID      = get_post_thumbnail_id(get_the_id());
            $image_default = wp_get_attachment_image_src($image_ID, 'full');
            $image_default = $image_default[0];
        } else {
            $image_default = get_template_directory_uri() . '/assets/img/logo.png';
        }
    }

    global $post;
    if (!is_404()) {
      $posttags = wp_get_post_tags( $post->ID );
    }
?>
</head>

     <!-- Código do Schema.org também para o Google+ -->
     <meta itemprop="name" content="<?php echo $title_default; ?>">
    <meta itemprop="description" content="<?php echo $desc_default; ?>">
    <meta itemprop="image" content="<?php echo $image_default; ?>">

    <!-- Open Graph Meta Data -->
    
    <!-- <meta name="p:domain_verify" content=""/> -->
    <meta property="og:title" content="<?php echo $title_default; ?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo get_home_url();?>"/>
    <meta property="og:image" content="<?php echo $image_default; ?>"/>
    <meta property="og:site_name" content="<?php echo get_bloginfo('name');?>"/>
    <!-- <meta property="fb:admins" content="USER_ID"/> -->
    <meta property="og:description"
        content="<?php echo $resumo ?>"/>

    <!-- Twitter Card -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@sejogacara">
    <meta name="twitter:title" content="<?php echo $title_default; ?>">
    <meta name="twitter:description" content="<?php echo $desc_default; ?>">
    <meta name="twitter:creator" content="@sejogacara">
    <meta name="twitter:image" content="<?php echo $image_default; ?>">
    <meta itemprop="name" content="<?php echo $title_default; ?>">
    <meta itemprop="description" content="<?php echo $desc_default; ?>">
    <meta itemprop="image" content="<?php echo $image_default; ?>">

<body <?php body_class(); ?>>
<div class="bodywrap">
    <div id="wrap" class="container">
        
<header>
    <div id="topbar">
        <div class="top-left">
            <input type="text" class="icon-search" placeholder="Pesquisar...">
            <i class="icon-search"></i>
        </div>
        <div class="top-right">
            <i class="icon-instagram"></i>
            <i class="icon-facebook"></i>
            <i class="icon-social-twitter"></i>
        </div>
    </div>
    <div id="cabecalho">
        <div class="logo">
            <img src="<? echo get_template_directory_uri()?>/assets/img/logo.png" alt="">
        </div>
        <div class="banner_topo">
              <img src="<? echo get_template_directory_uri()?>/assets/img/banner.gif" alt="">
        </div>
    </div>
    <nav id="menu">
    <?php  wp_nav_menu(
            array (
                'menu' => 'FreteUrbano',
                // 'walker'         => new WPSE_78121_Sublevel_Walker
            )
        ); 
    ?>
    </nav>
  <div class="noticias-home">
    <div class="stickslider">
      <div class="heading">
        <h3>Noticias</h3>
      </div>
      <ul class="lista">
      <li><i class="fas fa-angle-double-right"></i> Coluna de carro por aí: E a Caoa comprou a Chery</li>
      <li><i class="fas fa-angle-double-right"></i> Coluna de Carro Por Aí: JAC nascerá em Goiás ?</li>
      <li><i class="fas fa-angle-double-right"></i> Coluna de carro por aí: O preço do Fiat Cronos</li>
      <li><i class="fas fa-angle-double-right"></i> Coluna de carro por aí: O preço do Fiat Cronos</li>
      <li><i class="fas fa-angle-double-right"></i> O Bandeirante, a Hilux e a Arara Azul</li>
      </ul>
    </div>
  </div>
</header>