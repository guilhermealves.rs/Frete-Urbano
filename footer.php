<?php wp_footer(); ?>

<!-- ############# Rodapé ############### -->
<footer id="rodape">
<div class="logo">
            <img src="<? echo get_template_directory_uri()?>/assets/img/logo.png" alt="">
        </div>
</footer>

<!-- ############# Scripts ############### -->
<script>
    jQuery(function($){
        $(window).on('scroll', function() {
            scrollPosition = $(this).scrollTop();
            if (scrollPosition >= 50) {
                // If the function is only supposed to fire once
                $("#cabecalho > header").css('top', '0px');
                // Other function stuff here...
            }
            else if(scrollPosition <= 50 ) {
                $("#cabecalho > header").css('top', '30px');
            }
        });
    })
</script>
<script>
jQuery(function($){
 $(".owl-carousel").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2500,

   responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
 
  ]

});

  });
</script>

<script>
jQuery(function($){
 $(".slide-edicoes").slick({
  slidesToShow: 6,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2500,

   responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
 
  ]

});

  });
</script>

<script>
jQuery(function($){
$(document).ready(function() {
    $(window).on('load', function() {
        $('#pageloader').css('opacity', '0').delay(350).hide(0);
        $('body').css('overflow', 'auto');
        });
    });
})
</script>

<script type="text/javascript">
        jQuery(function(){
var feed = new Instafeed({
get: 'user',
  userId: '4159729019',
  limit: '6',
  accessToken: '4159729019.1677ed0.17d41b093f314df4a29e2c1e1923854e',
  template: '<div class="item"><a target="_new" href="{{link}}"><img src="{{image}}" /></a></div>'
});
feed.run();
})
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.12&appId=851797421618341&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

</body>
</html>
