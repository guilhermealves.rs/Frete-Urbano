<?php

get_header(); ?>

<div class="<?php global $post; echo $post->post_name;?> interna">
	</header>
	
	<div class="conteudo-interno-blog">
    <div class="<?php post_class() ?>">

<h1><?php the_title() ?></h1>

	<?php
		while ( have_posts() ) :
			the_post();
?>
        <div class="entry-thumb">
            <img class="img-destaque" src="<?php echo the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
        </div>

<header>
    
    <!-- <span class="entry-categories"><a href="http://www.revistafreteurbano.com.br/category/revista/colunistas/" rel="category tag">Colunistas</a></span>
    <span class="entry-date"><span class="kopa-minus"></span>26 de Fevereiro de 2018</span>
    <span class="entry-author">, por <a href="http://www.revistafreteurbano.com.br/author/flavia/" title="Posts de Flávia Gomes" rel="author">Flávia Gomes</a></span> -->

</header>

<?php the_content() ?>
<div class="share_btn">
<div class="button share-button facebook-share-button">share</div> <div class="button share-button twitter-share-button">tweet</div>
					<a href="http://www.facebook.com/sharer/sharer.php?s=100&p[title]=<?php echo urlencode(the_title()); ?>&p[url]=<?php echo urlencode(get_permalink()); ?>&p[summary]=<?php echo urlencode(the_excerpt()); ?>" target="popup" onclick="window.open('http://www.facebook.com/sharer/sharer.php?s=100&p[url]=http://www.facebook.com/sharer/sharer.php?s=100&p[title]=<?php echo urlencode(the_title()); ?>&p[url]=<?php echo urlencode(get_permalink()); ?>&p[summary]=<?php echo urlencode(the_excerpt()); ?>', 'popup', 'width=600,height=600'); return false;"><i class="fa fa-facebook" aria-hidden="true"></i></a>

				<a href="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>+<?php echo get_permalink(); ?>" target="popup" onclick="window.open('https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>+<?php echo get_permalink(); ?>', 'popup', 'width=600,height=600'); return false;"><i class="fa fa-twitter" aria-hidden="true"></i></a>

				<a href="https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>" target="popup" onclick="window.open('https://plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>', 'popup', 'width=600,height=600'); return false;"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
				</div>
<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
</div>

</div>
<div class="sidebar-interna">
   <div class="noticias">
   <div class="box">
			<div class="heading">
				<h2><a href="#">Publicidade</a></h2>
			</div>
		</div>
     <div class="box">
            <div class="heading">
                <h2><a href="#">Noticias</a></h2>
            </div>
			<div class="conteudo">
			<?php $loops = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 1 ) ); ?>
            <?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;
            ?>
                <div class="item destaque">
					<a href="<?php the_permalink()?>">	
                    <div class="img">
                        <img src="<?php the_post_thumbnail_url('medium-large')?>" alt="<?php the_title() ?>">
                    </div>
                    <div class="informacoes">
                        <div class="titulo">
                            <h2><?php the_title() ?></h2>
                        </div>
                        <p class="resumo">
                           <?php echo excerpt(25) ?>
                        </p>
					</div>
				</a>
                </div>
				<?php endwhile; wp_reset_query(); ?>
				<?php $loops = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 3, 'offset' => 1) ); ?>
				<?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;
				?>
                <div class="item">
					<a href="<?php the_permalink()?>">
                    <div class="img">
                        <img src="<?php the_post_thumbnail_url('medium-large')?>" alt="<?php the_title() ?>">
                    </div>
                    <div class="informacoes">
                        <div class="titulo">
                            <h2><?php the_title() ?></h2>
                        </div>
					</div>
					</a>
                </div>
				<?php wp_reset_query(); endwhile;?>
            </div>
        </div>

   </div>
</div>

</div>

    <div class="outros-posts">
        <?php

        $id = get_the_id(); 
        $adjacent = get_adjacent_post();
        $adjacent_id = $adjacent->ID;
        $next_post = get_next_post();
        $next_post_id = $next_post->ID;

        ?>
        <div class="item anterior">
            <a href="<?php the_permalink($adjacent_id); ?>"><span><i class="fas fa-angle-double-left"></i>Anterior</span><?php echo $adjacent->post_title ?></a>
            <a class="img" href="<?php the_permalink($adjacent_id); ?>">
            <img src="<?php echo get_the_post_thumbnail_url( $adjacent_id ); ?>" alt="<?php echo $adjacent->post_title ?>">
            </a>
        </div>

        <div class="item proximo">
            <a href="<?php the_permalink($next_post_id); ?>"><span><i class="fas fa-angle-double-right"></i>Próximo</span><?php echo $next_post->post_title ?></a>
            <a class="img" href="<?php the_permalink($next_post_id); ?>">
            <img src="<?php echo get_the_post_thumbnail_url( $next_post_id ); ?>" alt="<?php echo $next_post->post_title ?>">
            </a>
        </div>
    </div>

<?php
get_footer();