	<!-- ############# Edições ############### -->
	<div class="boxEdicoes">
		<div class="conteudo">
			<div class="item">
				<div class="edicao">
				<?php
				$taxonomyName = "category";
				//Could use ACF or basic custom field to get the "parent tax ID" dynamically from a page. At least that's what I would do.
				$parent_tax_ID = '1102';
				$parent_tax = get_term($parent_tax_ID);
				$primeiro = true;
				$terms = get_terms( $taxonomyName, array( 'parent' => $parent_tax_ID, 'orderby' => 'date', 'hide_empty' => false, 'posts_per_page' => 1 ) );
				foreach ( $terms as $term ) {
					if($primeiro) { ?>
			
					<div class="imagem" style="background:url('<?php echo z_taxonomy_image_url($term->term_id) ?>')">
						
					</div>
					<div class="heading">
					
					</div>
					<footer><h2><?php echo $term->name ?> </h2></footer>
				</div>
				<?php $primeiro = false; } } ?>
			</div>
			<div class="item principal">
				<div class="edicao atual">
					<div class="imagem" style="background:url('<?php echo get_template_directory_uri() ?>/assets/img/edicao/3.jpg')">
						
					</div>
				<div class="heading">
					<div class="edicao">
						
					</div>
					<!-- <div class="mesLancamento">
						<h3>Dezembro</h3>
					</div> -->
				</div>
					<footer><h2>Coluna de carro por aí: O preço do Fiat Cronos</h2></footer>
				</div>
			</div>
			<div class="item">
				<div class="edicao">
					<div class="imagem" style="background:url('<?php echo get_template_directory_uri() ?>/assets/img/edicao/4.jpg')">
						
					</div>
					<div class="heading">
						<div class="mesLancamento">
						<h3>Setembro</h3>
						</div>
					</div>
					<footer><h2>Coluna De carro por aí: Registro: há 132 anos surgia o automóvel</h2></footer>
				</div>
				<div class="edicao">
					<div class="imagem" style="background:url('<?php echo get_template_directory_uri() ?>/assets/img/edicao/5.jpg')">
						
					</div>
					<div class="heading">
						<div class="mesLancamento">
						<h3>Agosto</h3>
						</div>
					</div>
					<footer><h2>O Bandeirante, a Hilux e a Arara Azul</h2></footer>
				</div>
			</div>
		</div>
	</div>