
<header>
  <h2><a href="#">Edição Atual</a></h2>
</header>

<div class="destaque owl-carousel">
  <?php $loops = new WP_Query( array( 'post_type' => 'revista', 'posts_per_page' => 20 ) ); ?>
  <?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;
  ?>
	<div class="item">
    <a href="<?php the_permalink() ?>">
		<div class="box">
      <div class="imagem">
        <img src="<?php the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
      </div>
      <h2><?php the_title() ?></h2>
      <div class="data"><?php echo get_the_date('j F, Y'); ?></div>
    </div>
    </a>
  </div>
  <?php endwhile; ?>
</div>