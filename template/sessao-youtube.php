<!-- ############# Lançamentos ############### -->
<div class="osDestacados conteudo-blog">
	<header>
		<h2><a href="#">Vídeos</a></h2>
	</header>
	<div class="conteudo">
	<div class="itemDestaque youtube">
<!-- <div class="btnPlay">
	<img src="<?php echo get_template_directory_uri() . '/assets/img/play.png' ?>" alt="">
</div> -->
<?php $loops = new WP_Query( array( 'post_type' => 'video', 'posts_per_page' => 1 ) ); ?>
<?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;
?>
<a href="<?php the_permalink() ?>">
	<div class="img" style="background: url('<? echo the_post_thumbnail_url('medium-large') ?>');">
	</div>
	<div class="descricao">
		<h1><?php the_title()?></h1>
		<div class="resumo">
		<?php echo excerpt(25)?>
		</div>
	</div>
</a>
</div>
<?php endwhile; ?>
<div class="itemDestaque">
<?php $loops = new WP_Query( array( 'post_type' => 'video', 'posts_per_page' => 3, 'offset' => 1 ) ); ?>
<?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;
?>
<div class="item">
	<a href="<?php the_permalink() ?>">
		<img src="<?php echo the_post_thumbnail_url('thumbnail')?>" alt="<?php the_title() ?>">
	<div class="box">
		<div class="heading">
				<h1><?php the_title() ?></h1>
		</div>
		<div class="postInfo">
			<div class="data">
			<?php echo get_the_date('j F, Y'); ?>
			</div>
			<div class="autor">
			<?php the_author() ?>
			</div>
		</div>
	</div>
	</a>
</div>
<?php endwhile ?>
		<a class="link" href="#">Ver todos</a>
	</div>
</div>
</div>
