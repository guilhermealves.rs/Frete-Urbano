<div class="sidebar-interna">
   <div class="noticias">
			<div class="heading">
				<h2><a href="#">Publicidade</a></h2>
            </div>

	<section id="publicidadeSidebar">
		<div class="box">
			<img src="http://place-hold.it/330x110/" alt="">
		</div>
		
		<div class="box">
			<img src="http://place-hold.it/330x240/" alt="">
		</div>
	</section>
     <div class="box">
            <div class="heading">
                <h2><a href="#">Noticias</a></h2>
            </div>
			<div class="conteudo">
			<?php $loops = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 1, 'category__not_in' => array( 1102 ), 'taxt_query' => array(
                  array(
                      'include_children' => false
                  )
              ) ) ); ?>
            <?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;
            ?>
                <div class="item destaque">
					<a href="<?php the_permalink()?>">	
                    <div class="img">
                        <img src="<?php the_post_thumbnail_url('medium-large')?>" alt="<?php the_title() ?>">
                    </div>
                    <div class="informacoes">
                        <div class="titulo">
                            <h2><?php the_title() ?></h2>
                        </div>
                        <p class="resumo">
                           <?php echo excerpt(25) ?>
                        </p>
					</div>
				</a>
                </div>
				<?php endwhile; wp_reset_query(); ?>
				<?php $loops = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 3, 'offset' => 1, 'category__not_in' => array( 1102 ), 'taxt_query' => array(
                  array(
                      'include_children' => false
                  )
              )) ); 
              ?>
				<?php while ( $loops->have_posts() ) : $loops->the_post(); global $post;
				?>
                <div class="item">
					<a href="<?php the_permalink()?>">
                    <div class="img">
                        <img src="<?php the_post_thumbnail_url('medium-large')?>" alt="<?php the_title() ?>">
                    </div>
                    <div class="informacoes">
                        <div class="titulo">
                            <h2><?php the_title() ?></h2>
                        </div>
					</div>
					</a>
                </div>
				<?php endwhile;?>
            </div>
        </div>

   </div>
</div>