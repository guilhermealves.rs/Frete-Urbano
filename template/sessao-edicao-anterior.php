<!-- ############# Lançamentos ############### -->
<div class="edicoes-anteriores conteudo-blog">
	<header>
		<h2><a href="#">Edições anteriores</a></h2>
	</header>
	<div class="conteudo">
        <div class="slide-edicoes">
        <?php
    $taxonomyName = "category";
    //Could use ACF or basic custom field to get the "parent tax ID" dynamically from a page. At least that's what I would do.
    $parent_tax_ID = '1102';
    $parent_tax = get_term($parent_tax_ID);
    $terms = get_terms( $taxonomyName, array( 'parent' => $parent_tax_ID, 'orderby' => 'date', 'hide_empty' => false ) );
    foreach ( $terms as $term ) {
        $data = get_field('data_edicao', $term);
        echo '<div class="item">'.
        '<article>'.
        '<a href="' . get_term_link( $term ) . '">'.
        '<div class="img">'.
        '<img src="' . z_taxonomy_image_url($term->term_id) . '">'.
        '</div>'.
        '<div class="descricao">'.
        '<div class="heading">'.
        '<span>' . date_i18n("d F, Y", strtotime($data)) . '</span>'.
        '</div>'.
        '</a>'.
        '</article>'.
        '</div>';
    }
?>
        </div>
    </div>
</div>