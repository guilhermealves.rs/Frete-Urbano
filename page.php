<?php

get_header(); ?>

<div class="page-<?php global $post; echo $post->post_name;?> interna categoria">
	
	<div class="conteudo-interno-blog">
 
    <?php
    if(is_page('videos')){
        $loops = new WP_Query( array( 'post_type' => 'video', 'posts_per_page' => -1 ) );
        
    }
    else {
		$loops = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => -1 ) );
        }
        while ( $loops->have_posts() ) : $loops->the_post(); global $post;
?>
        <article class="<?php post_class() ?>">
            <h1><?php the_title() ?></h1>
            <div class="conteudo">
                <div class="img">
                    <?php echo the_post_thumbnail( 'medium' ) ?>
                </div>
                <div class="resumo">
                    <?php the_excerpt() ?>
                    <a href="<?php the_permalink() ?>">Leia mais</a>
                </div>
            </div>
        </article>
    <?php endwhile; ?>


    </div>
    <?php get_template_part('template/sidebar', 'interna') ?>
</div>
<?php
get_footer();